﻿using ArchiveDecryptor.Infrastructure.Classes;
using ArchiveDecryptor.Infrastructure.Views.Pages.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArchiveDecryptor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Task BFTask;

        public BruteForce BruteForce { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            BruteForce = new BruteForce();
            
            this.DataContext = BruteForce;
        }

        private void ForceButton_Click(object sender, RoutedEventArgs e)
        {
            IncomingData.IsEnabled = false;
            ForceButton.IsEnabled = false;

            BFTask = Task.Run(async () =>
            {
                if (await BruteForce.Start())
                {
                    if (BruteForceSettings.IsAppSounds)
                        SystemSounds.Beep.Play();

                    BruteForce.CurrentResult = BruteForce.DecryptedPassword;
                    Dispatcher.Invoke(() => 
                    {
                        IncomingData.IsEnabled = true;
                        ForceButton.IsEnabled = true;
                    });
                }
            });
        }

        private void IncomingData_TextChanged(object sender, TextChangedEventArgs e)
        {
            BruteForce.MyData = IncomingData.Text;
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsPage settingsPage = new SettingsPage();
            SettingsFrame.Content = settingsPage;
            SettingsFrame.Visibility = Visibility.Visible;
        }
    }
}
