﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveDecryptor.Infrastructure.ViewModels.Settings
{
    public class SettingsViewModel
    {
        public bool IsLowerCase { get; set; }
        public bool IsUpperCase { get; set; }
        public bool IsNumbers { get; set; }
        public bool IsSpecialCharacters { get; set; }
        public bool IsDictionaries { get; set; }
        public bool IsAppSounds { get; set; }
    }
}
