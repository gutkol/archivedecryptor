﻿using ArchiveDecryptor.Infrastructure.Classes.Archives;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveDecryptor.Infrastructure.Classes
{
    public class BruteForce : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion INotifyPropertyChanged implementation

        string decryptedPassword = null;
        string currentResult = null;
        bool isMatched = false;

        List<Task> charTasks = null;
        static readonly ZipDecrypt zipDecrypt = new ZipDecrypt(@"C:\Users\Radek\Desktop\a.zip");

        public string DecryptedPassword
        { 
            get
            {
                return decryptedPassword;
            }
            set
            {
                decryptedPassword = value;
                NotifyPropertyChanged("DecryptedPassword");
            }
        }

        public string CurrentResult
        { 
            get
            {
                return currentResult;
            }
            set
            {
                currentResult = value;
                NotifyPropertyChanged("CurrentResult");
            }
        }

        public string MyData { get; set; }

        public async Task<bool> Start()
        {
            ClearObjects();

            int estimatedPasswordLength = 0;

            while (!isMatched)
            {
                //await StartBF(++estimatedPasswordLength);
                await StartBFMany(++estimatedPasswordLength);
            }

            return true;
        }

        private async Task StartBF(int keyLength)
        {
            var keyChars = await CreateCharArray(keyLength, BruteForceSettings.Characters[0]);
            // The index of the last character will be stored for slight perfomance improvement
            var indexOfLastChar = keyLength - 1;
            await CreateNewKey(0, keyChars, keyLength, indexOfLastChar);
        }

        private async Task<char[]> CreateCharArray(int length, char defaultChar)
        {
            return new char[length].Select(c => defaultChar).ToArray();
        }

        private async Task CreateNewKey(int currentCharPosition, char[] keyChars, int keyLength, int indexOfLastChar)
        {
            var nextCharPosition = currentCharPosition + 1;
            // We are looping trough the full length of our charactersToTest array
            for (int i = 0; i < BruteForceSettings.Characters.Length; i++)
            {
                /* The character at the currentCharPosition will be replaced by a
                 * new character from the charactersToTest array => a new key combination will be created */
                keyChars[currentCharPosition] = BruteForceSettings.Characters[i];

                if (isMatched) return;
                // The method calls itself recursively until all positions of the key char array have been replaced
                if (currentCharPosition < indexOfLastChar)
                {
                    await CreateNewKey(nextCharPosition, keyChars, keyLength, indexOfLastChar);
                }
                else
                {
                    string currentKeyChars = new string(keyChars);
                    CurrentResult = currentKeyChars;
                    if (await zipDecrypt.Decrypt(currentKeyChars))
                    {
                        isMatched = true;
                        DecryptedPassword = CurrentResult;
                    }
                    /* The char array will be converted to a string and compared to the password. If the password
                     * is matched the loop breaks and the password is stored as result. */
                    //if (MyData == CurrentResult)
                    //{
                    //    isMatched = true;
                    //    DecryptedPassword = CurrentResult;

                    //    return;
                    //}
                }
            }
        }

        private async Task StartBFMany(int keyLength)
        {

            // The index of the last character will be stored for slight perfomance improvement
            var indexOfLastChar = keyLength - 1;
            charTasks = new List<Task>();

            for (int character = 0; character < BruteForceSettings.Characters.Length; character++)
            {
                var keyChars = await CreateCharArrayMany(keyLength, BruteForceSettings.Characters[character],
                    BruteForceSettings.Characters[0]);

                Task firstCharacterTask = new Task(async () =>
                {
                    await CreateNewKeyMany(0, keyChars, keyLength, indexOfLastChar);
                });
                firstCharacterTask.Start();
                charTasks.Add(firstCharacterTask);
            }

            Task.WaitAll(charTasks.ToArray());
        }

        private async Task<char[]> CreateCharArrayMany(int length, char firstSearchChar, char defaultChar)
        {
            char[] charArray = new char[length].Select(c => defaultChar).ToArray();
            charArray[0] = firstSearchChar;

            return charArray;
        }

        private async Task CreateNewKeyMany(int currentCharPosition, char[] keyChars, int keyLength, int indexOfLastChar)
        {
            var nextCharPosition = currentCharPosition + 1;
            // We are looping trough the full length of our charactersToTest array
            for (int i = 0; i < BruteForceSettings.Characters.Length; i++)
            {
                /* The character at the currentCharPosition will be replaced by a
                 * new character from the charactersToTest array => a new key combination will be created */
                keyChars[currentCharPosition] = currentCharPosition == 0 ? keyChars[currentCharPosition] : BruteForceSettings.Characters[i];

                if (isMatched) return;
                // The method calls itself recursively until all positions of the key char array have been replaced
                if (currentCharPosition < indexOfLastChar)
                {
                    await CreateNewKeyMany(nextCharPosition, keyChars, keyLength, indexOfLastChar);
                }
                else
                {
                    string currentKeyChars = new string(keyChars);
                    CurrentResult = currentKeyChars;

                    if (await zipDecrypt.Decrypt(currentKeyChars))
                    {
                        isMatched = true;
                        DecryptedPassword = currentKeyChars;

                        return;
                    }

                    /* The char array will be converted to a string and compared to the password. If the password
                     * is matched the loop breaks and the password is stored as result. */

                    //if (MyData == currentKeyChars)
                    //{
                    //    isMatched = true;
                    //    DecryptedPassword = currentKeyChars;

                    //    return;
                    //}
                }

                if (currentCharPosition == 0)
                    break;
            }
        }

        private void ClearObjects()
        {
            DecryptedPassword = null;
            CurrentResult = null;
            isMatched = false;
            charTasks = null;
        }
    }
}
