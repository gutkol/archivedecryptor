﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveDecryptor.Infrastructure.Classes
{
    static class BruteForceSettings
    {
        static readonly char[] allCharacters = Enumerable.Range(32, 95).Select(x => (char)x).ToArray();
        static readonly char[] lowerAlphabet = Enumerable.Range('a', 26).Select(x => (char)x).ToArray();
        static readonly char[] upperAlphabet = Enumerable.Range('A', 26).Select(x => (char)x).ToArray();
        static readonly char[] numbers = Enumerable.Range('0', 10).Select(x => (char)x).ToArray();
        static readonly char[] specialCharacters = allCharacters.Except(lowerAlphabet).Except(upperAlphabet).Except(numbers).ToArray();

        static public bool IsLowerCase { get; set; } = true;
        static public bool IsUpperCase { get; set; } = true;
        static public bool IsNumbers { get; set; } = true;
        static public bool IsSpecialCharacters { get; set; } = true;
        static public bool IsDictionaries { get; set; }
        static public bool IsAppSounds { get; set; } = true;
        static public char[] Characters
        {
            get
            {
                return GetCharacters();
            }
        }

        static private char[] GetCharacters()
        {
            if (IsLowerCase && IsUpperCase && IsNumbers && IsSpecialCharacters)
            {
                return Enumerable.Range(32, 95).Select(x => (char)x).ToArray();
            }
            else
            {
                List<char> characters = new List<char>();

                if (IsLowerCase)
                    characters.AddRange(lowerAlphabet);
                if (IsUpperCase)
                    characters.AddRange(upperAlphabet);
                if (IsNumbers)
                    characters.AddRange(numbers);
                if (IsSpecialCharacters)
                    characters.AddRange(specialCharacters);

                return characters.ToArray();
            }
        }
    }
}
