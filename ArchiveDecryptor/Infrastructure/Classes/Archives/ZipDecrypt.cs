﻿using ArchiveDecryptor.Infrastructure.Interfaces.Archives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;

namespace ArchiveDecryptor.Infrastructure.Classes.Archives
{
    class ZipDecrypt : IArchiveDecryptor
    {
        string filePath = null;

        public ZipDecrypt(string filePath)
        {
            this.filePath = filePath;
        }

        public async Task<bool> Decrypt(string password)
        {
            try
            {
                var task = Task.Run(() =>
                {
                    try
                    {
                        using (ZipFile archive = ZipFile.Read(filePath))
                        {
                            archive.Password = password;
                            archive.ExtractAll(Path.Combine(Path.GetDirectoryName(@"C:\Users\Radek\Desktop\ssn postgres.zip"), "decrypt"),
                                ExtractExistingFileAction.OverwriteSilently);
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                });

                return task.Result;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }
    }
}
