﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveDecryptor.Infrastructure.Interfaces.Archives
{
    interface IArchiveDecryptor
    {
        public Task<bool> Decrypt(string password);
    }
}
