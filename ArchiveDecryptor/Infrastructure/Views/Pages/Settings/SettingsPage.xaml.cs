﻿using ArchiveDecryptor.Infrastructure.Classes;
using ArchiveDecryptor.Infrastructure.ViewModels.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArchiveDecryptor.Infrastructure.Views.Pages.Settings
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page
    {
        public SettingsViewModel SettingsView { get; set; }

        public SettingsPage()
        {
            InitializeComponent();

            SettingsView = new SettingsViewModel();
            MapBruteForceSettings();

            this.DataContext = SettingsView;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            BruteForceSettings.IsLowerCase = SettingsView.IsLowerCase;
            BruteForceSettings.IsUpperCase = SettingsView.IsUpperCase;
            BruteForceSettings.IsNumbers = SettingsView.IsNumbers;
            BruteForceSettings.IsSpecialCharacters = SettingsView.IsSpecialCharacters;

            Visibility = Visibility.Hidden;
        }

        private void MapBruteForceSettings()
        {
            SettingsView.IsLowerCase = BruteForceSettings.IsLowerCase;
            SettingsView.IsUpperCase = BruteForceSettings.IsUpperCase;
            SettingsView.IsNumbers = BruteForceSettings.IsNumbers;
            SettingsView.IsSpecialCharacters = BruteForceSettings.IsSpecialCharacters;
        }
    }
}
